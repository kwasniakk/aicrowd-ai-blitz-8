# AIcrowd AI Blitz 8
## 🕵️Problem description
Mercedes-AMG and Red Bull Racing are two of the fiercest rivals in recent times. They are constantly trying to one-up each other and gain a significant advantage. They often spend lots of time and research in developing the best car chassis (body) to get that edge on the track. For this puzzle, you’re given images of the F1 car chassis (body) and based on that you need to classify whether the car body belongs to Red Bull or Mercedes.
## 💾 Dataset
The given dataset contains images of two different F1 Teams i.e. Redbull and Mercedes of size 256*256 in jpg format. The images in train.zip and val.zip  have their labels i.e. which team it is in train.csv and val.csv. The labels for the images in test.zip needs to be predicted.
### Activate virtual environment
```bash
python3 -m venv aicrowd
source aicrowd/bin/activate
```
### Install requirements
```bash 
cd src
pip3 install -r requirements.txt
```
### Organize files
Run all cells in `notebooks/f1-organize-files.ipynb`.


#### Links
* [Project Page](https://wandb.ai/kwasniakk/Computational%20Intelligence%20Project)
* [Report](https://wandb.ai/kwasniakk/Computational%20Intelligence%20Project/reports/Computational-Intelligence-Project--Vmlldzo3OTU4NTQ)
* [LinkedIn](https://www.linkedin.com/in/kwasniak-krzysztof)