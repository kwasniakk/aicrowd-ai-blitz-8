from __future__ import annotations

import io
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
import PIL
import seaborn as sns
import torch
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score


class EpochMetrics:
    def __init__(self, dataset_len, phase: str):
        self.loss = 0
        self.predictions = torch.zeros(0, dtype=torch.long)
        self.labels = torch.zeros(0, dtype=torch.long)
        self.dataset_len = dataset_len
        self.phase = phase
        self.metrics_to_measure = {
            "Accuracy": accuracy_score,
            "F1 Score": f1_score,
            "Confusion Matrix": confusion_matrix,
        }

    def update(self, loss_value: torch.Tensor, preds: torch.Tensor, labels: torch.Tensor):
        self.loss += loss_value.item()
        _, preds = torch.max(preds, 1)
        self.predictions = torch.cat([self.predictions, preds.view(-1).cpu()])
        self.labels = torch.cat([self.labels, labels.view(-1).cpu()])

    def to_dict(self):
        metrics = {
            f"{self.phase} {metric_name}": metric_function(
                self.labels.detach().numpy(), self.predictions.detach().numpy()
            )
            for metric_name, metric_function in self.metrics_to_measure.items()
        }
        metrics[f"{self.phase} Loss"] = self.loss / self.dataset_len
        return metrics


def get_confusion_matrix_matplotlib(metrics: dict, classes: list[str]):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 9), dpi=70)
    sns.heatmap(
        metrics["Training Confusion Matrix"],
        annot=True,
        fmt="d",
        cmap="viridis",
        xticklabels=classes,
        yticklabels=classes,
        ax=ax1,
    )
    ax1.set_title("Training")
    sns.heatmap(
        metrics["Validation Confusion Matrix"],
        annot=True,
        fmt="d",
        cmap="viridis",
        xticklabels=classes,
        yticklabels=classes,
        ax=ax2,
    )
    ax2.set_title("Validation")
    return matplotlib_to_PIL(fig)


def matplotlib_to_PIL(fig: matplotlib.Figure):
    buf = io.BytesIO()
    fig.savefig(buf)
    buf.seek(0)
    plt.close(1)
    plt.close(2)
    return PIL.Image.open(buf)
